package com.rtwo.simplenote.model;

import com.jfinal.plugin.activerecord.Model;

public class NoteModel extends Model<NoteModel>{

	private static final long serialVersionUID = 1L;
	public static final NoteModel noteTable = new NoteModel();
}
