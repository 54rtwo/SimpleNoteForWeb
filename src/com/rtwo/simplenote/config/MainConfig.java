package com.rtwo.simplenote.config;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;
import com.rtwo.simplenote.controller.MainController;
import com.rtwo.simplenote.interceptor.GlobalInterceptor;
import com.rtwo.simplenote.model.NoteModel;
import com.rtwo.simplenote.model.UserModel;

public class MainConfig extends JFinalConfig{

	public static String mysqlUrl;
	public static String userName;
	public static String passWord;
	public static boolean devMode;
	
	/**
	 * 进行常量的配置，开发模式，视图模式，编码方式
	 */
	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true);
		me.setViewType(ViewType.JSP);
		me.setEncoding("utf-8");
	}

	/**
	 * 指定路由地址
	 */
	@Override
	public void configRoute(Routes me) {
		me.add("/", MainController.class,"/root");
	}

	/**
	 * 进行数据库连接的配置,addMapping的第一个参数必须和对应的table的单词一致,第二个参数表示
	 * 如果这个table的主键不是"id",那么需要指明,如果是,则省略该参数
	 */
	@Override
	public void configPlugin(Plugins me) {
		Prop dbConfig = PropKit.use("db.config");
		mysqlUrl = dbConfig.get("mysqlUrl");
		userName = dbConfig.get("userName");
		passWord = dbConfig.get("passWord");
		C3p0Plugin c3p = new C3p0Plugin(mysqlUrl, userName, passWord);
		me.add(c3p);
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p);
		me.add(arp);
		arp.addMapping("user","Id", UserModel.class);
		arp.addMapping("note","noteId", NoteModel.class);
	}

	/**
	 * 全局拦截器的声明
	 */
	@Override
	public void configInterceptor(Interceptors me) {
//		me.add(new GlobalInterceptor());
	}

	@Override
	public void configHandler(Handlers me) {
		
	}

	public static void main(String[] args){
		JFinal.start("WebRoot",8080,"/",5);
	}
}
