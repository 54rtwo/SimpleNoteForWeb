<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
	<script src="http://apps.bdimg.com/libs/jquery/2.0.0/jquery.min.js"></script>
	<script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<title>用户界面</title>
</head>
<body>
	<c:set var="user" value="${userInfo}" scope="request"/>
	<c:if test="${user!=null}">
		<h2><strong><span style="color: rgb(255, 0, 0)">${user.userName}</span></strong>,欢迎你</h2>
		<h3><a href="/logout">注销</a></h3>
		<h3>你一共写了<c:out value="${user.notesNumber}"/>篇笔记&nbsp;&nbsp;<a href="/writeNotes">写笔记</a></h3>
		<hr/>
		<h3>笔记列表</h3>
		<ol>
		<c:forEach var="item" items="${noteInfo}">
			<li style="margin-left:0px ; margin-right: 20px" ><a style="padding-right:100px" href="/showNotes/${item.noteId}">${item.noteName}</a>&nbsp;<a style="color:#FF0000" href="/deleteNotes/${item.noteId}">删除</a></li>
		</c:forEach>
		</ol>
	</c:if>
	<c:if test="${user.userName==null}">
		<h2>你没有进行登录,请<a href="/">登录</a></h2>
	</c:if>
</body>
</html>