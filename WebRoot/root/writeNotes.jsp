<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
<script src="http://apps.bdimg.com/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
		$("button").click(function() {
			$.post("/saveNotes", {
				"noteTitle" : $("#noteTitle").val(),
				"noteContent" : UE.getEditor('editor').getContent(),
				"userId" : $("#userId").val()
			}, function(data, status){
				if (data.info=="sucess"){
					window.location.assign("/userInterface");
				} 
				else{
					alert("保存失败");
				}
			},
			"json");
		});
	});
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑笔记</title>
</head>
<body>
	<c:set var="user" value="${userInfo}" scope="request" />
	<c:if test="${user!=null}">
		<pre style="font-size: 16px">标题: <input size="25"
				style="border: 0px; border-bottom: #000000 1px solid" type="text"
				id="noteTitle" name="noteTitle" />
		</pre>
		<input id="userId" type="hidden" value="${user.userId}" />
		<jsp:include page="/ueditor/index.html"></jsp:include>
		<br />
		<button name="save" class="button button-glow button-rounded button-raised button-primary">保存</button>
	</c:if>
	<c:if test="${user.userName==null}">
		<h2>
			你没有进行登录,请<a href="/">登录</a>
		</h2>
	</c:if>
</body>
</html>